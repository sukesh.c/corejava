//  Hash table in Java :public class Hashtable<K,V> extends Dictionary<K,V> implements Map<K,V>, Cloneable, Serializable
		//This class implements a hash table, which maps keys to values. Any non-null object can be used as a key or as a value.
		//An instance of HashTable has two parameters that affect its performance: initial capacity and load factor
		//Initial Capacity -  capacity it allocate  initially -11
		//Load Factor :how full the hash table is allowed to get before its capacity is automatically increased
		//the default load factor (.75) offers a good tradeoff between time and space costs. Higher values decrease the space overhead but increase the time cost to look up an entry (which is reflected in most Hashtable operations, including get and put).
        // Properties class is subclass of  HashTable 
        // Methods : put, get 
/*
 * Hashtable()
Constructs a new, empty hashtable with a default initial capacity (11) and load factor (0.75).
Hashtable(int initialCapacity)
Constructs a new, empty hashtable with the specified initial capacity and default load factor (0.75).
Hashtable(int initialCapacity, float loadFactor)
Constructs a new, empty hashtable with the specified initial capacity and the specified load factor.
Hashtable(Map<? extends K,? extends V> t)
Constructs a new hashtable with the same mappings as the given Map.
 */

package corejava;

import java.util.Hashtable;

public class HashTableDemo {

	public HashTableDemo() {
		
		
	}
public static void main(String args[]) {
	Hashtable<Integer,String> ht =new Hashtable();
	ht.put(1,"sukesh");
	ht.put(2, "chulliyote");
	ht.put(3, "oracle");
	ht.put(1,"kesh"); // update of exsisitng key value
	ht.putIfAbsent(6,"kes" ); // This  will  not  work as  1 key already there 
	//ht.put(null, "test"); this will generate  java.lang.NullPointerException and  null  key is not supported
	System.out.println(ht.get(1));
	Hashtable<Integer,String> ht1 =new Hashtable<Integer, String>(5,(float) 0.9); // initial capacity 5 and LF 0.9
	ht1.put(1,"sukesh");
	ht1.put(2, "chulliyote");
	ht1.put(3, "oracle");
	ht1.put(4,"kesh"); 
	ht1.put(5,"lex");
	System.out.println(ht1.get(1));
	Hashtable<Integer,String> ht2 =new Hashtable<Integer, String>();
	System.out.println("Size:  "+ht2.size());
	System.out.println("Size:  "+ht2.remove(1));
	System.out.println(ht2.get(1));
}
 
}
