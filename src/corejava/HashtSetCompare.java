package corejava;

import java.util.HashSet;
import java.util.Hashtable;
/* In this demo,  Hash set  should not allow duplicate  values as it is part of Set  Interface However it 
 *  is allowing .the reason is  when you implement HashSet , we need to over ride  hashCode and equals to methods 
 * If we are not doing it  , it uses  those method  from superclass  of all class  ( Object class)
 * where  this was implemented  . 
 * Hashcode implemented in Object Class  is  just generating a random number .so  each instances will have different  hashCode  value 
 * 
 * See the hashcode for and  .equals  outupt for instances with same values  ( here  cd1 and  cd7)
 * uncomment  commented  part  in CarDetails .java it will show  why hashcode and  equalis to  override is necessary while using  
 *  Hashset
 * 
 * 
 * */

public class HashtSetCompare {

	public HashtSetCompare() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	HashSet <CarDetails> cars = new HashSet <CarDetails>();
	
     CarDetails cd1= new CarDetails ("Ford",1500000);
     CarDetails cd2=new CarDetails ("Cheverlet",180000);
     CarDetails cd3=new CarDetails ("Honda",1600000);
     CarDetails cd4= new CarDetails ("Nissan",1500000);
     CarDetails cd5=new CarDetails ("TATA",1500000);
     CarDetails cd6=new CarDetails (null,1500000);
     CarDetails cd7 = new CarDetails ("Ford",1500000);
     cars.add(cd1);
     cars.add(cd2);
     cars.add(cd3);
     cars.add(cd4);
     cars.add(cd5);
     cars.add(cd6);
     cars.add(cd7);
     for  (CarDetails carDetails  : cars) {
    	 System.out.println(carDetails);
     }
     System.out.println("Checking Equals " + cd1.equals(cd7)); //false becuse  cd1 and  cd7 has  differnt  hash codes 
     System.out.println("Hash Code for  cd1 :"+cd1.hashCode());
     System.out.println("Hash Code for  cd 7 :"+cd7.hashCode());
}
}
