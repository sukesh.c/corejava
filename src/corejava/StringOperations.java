package corejava;
/*
 * 
 *  This  Program covers  most  imp methods  in String and  String  Tokenizer 
public final class String extends Object implements Serializable, Comparable<String>, CharSequence
 * Good Read about why strings are immutable :https://java-journal.blogspot.com/2010/12/why-strings-in-java-are-immutable.html
 * String  Important methods : https://docs.oracle.com/javase/8/docs/api/java/lang/String.html check this  java doc 
 * https://docs.oracle.com/javase/8/docs/api/java/util/StringTokenizer.html
 * 
 * 
 * */
import java.util.StringTokenizer;

public class StringOperations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str1="Sukesh Chulliyote";
		char data[] = {'o', 'r', 'a','c','l','e'};
		String dataString= new String(data);
		System.out.println("Char array {'o', 'r', 'a','c','l','e'}  to string   :  "+dataString);
		String str2 = new String ();
		  System.out.println(str2);
		  System.out.println("Length of string using  str.lengh():  "+str1.length());
		  System.out.println("Index Of String  usng  str1.IndexOf:  "+str1.indexOf('u'));
	      System.out.println("HashCode of  String  using str1.Hashcode:  "+str1.hashCode());
		  System.out.println("CharAt particular  index  using str1.CharAt(2) for Sukesh :    "+str1.charAt(2));
		  
		  str2="oracle,india,private,limited";
		StringTokenizer st1=new StringTokenizer(str1); // Default  tokenizer  is  space
		String s1 = str1.replaceAll("\\s", ""); 
		System.out.println((st1).nextToken());
		StringTokenizer st2=new StringTokenizer(str2,",");
		while(st2.hasMoreTokens())
		System.out.println((st2).nextToken());
	}

}
