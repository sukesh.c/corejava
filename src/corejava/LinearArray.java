package corejava;

import java.util.Arrays;

/*Liner Array aka  single dimension array demo */
public class LinearArray {
    public static void main(String[] args) {
        //1: 2 ways  to initialize the arrays
        int a1[] = {12,14,11,18,19,32,23};
        int a0 [] =new int[10];
        a0= new int[]{5, 6, 3, 2, 93};        ;

        //2.get Legnth of Array
        System.out.println("Length of  first Array A1 : "+a1.length);
        //3 Access Element
        System.out.println("First Element : "+a1[0]);

        //4 Iterate through an array
        System.out.println("Version 1 :Old the contents in the array are : ");
        for (int i=0;i<a1.length;i++){
            System.out.println("  "+ a1[i]);

        }
        // 5: new way  iteration
        System.out.println("Version 2 :  for each the contents in array are ");
        for (int item :a1){
            System.out.println("   "+item);
            }
            // 6 Modify the  element
    a1[0] =31;

        Arrays.sort(a1);
        System.out.println("Sorted Values  ");
        for (int item :a1)
            System.out.println("   "+item);


}
}
