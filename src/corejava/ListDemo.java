package corejava;

import java.util.ArrayList;
import java.util.List;

/*
 *List Interface and its ArrayList implementation
 *The Java.util.List is a child interface of Collection. 
 *It is an ordered collection of objects in which duplicate values can be stored. 
 *List preserves the insertion order, it allows positional access and insertion of elements.
 *List Interface is implemented by ArrayList, 
 *LinkedList, Vector and Stack classes. This is Child Interface of  Collection so Supports all 
 * collection methonds 
 * List Methods 
 * void add(int index,Object O): Add element at specified index.
 * 	boolean addAll(int index, Collection c):  adds all elements from specified collection to list.
 *  First element gets inserted at given index. If there is already an element at that position, that element and other subsequent elements(if any) are shifted to the right by increasing their index.
 *  Object remove(int index): This method removes an element from the specified index. It shifts subsequent elements(if any) to left and decreases their indexes by 1.
 *  Object get(int index): This method returns element at the specified index.
 *  Object set(int index, Object new): This method replaces element at given index with new element. This function returns the element which was just replaced by new element.
 * 
 * */

public class ListDemo {

	public ListDemo() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
   List <Object>li=new ArrayList<Object>();
   li.add('a');
   li.add('b');
   li.add('c');
   li.add('d');
   li.add('e');
   li.add('d');
   li.add('c');
   li.add('d');
   li.add('e');
   li.add('d');
   li.set(1, 'X');
   li.add(3, 'z');
   li.add('m');
   System.out.println("Print the  List Initially : "+li);
   li.set(2,null); // null can be added  in list 
   System.out.println("Print the  List After set null to pos 2 list.set(2,null)  : "+li);
   li.add('d');
   System.out.println("Print the  List After list.add : "+li);
   li.add(7,"q");
   System.out.println("Print the  List After list.Add(7,q) : "+li);
   li.add(null);
   System.out.println("Print the  List After list.add null : "+li);
   
   List<Object> newList = new ArrayList<Object>();
   newList.add("S");
   newList.add("U");
   
   System.out.println("Print the  New List : "+newList);
   newList.addAll(li);
   System.out.println("Print the  New List after adding all of first : "+newList);
   
   System.out.println("list.indexOf() : list first occurance "+li.indexOf('d'));
   System.out.println("list.indexOf() : for a null "+li.indexOf(null));
   System.out.println("list.indexOf() : Last Index"+li.lastIndexOf('d'));
   System.out.println("list.size() :"+li.size());
   List li1=newList.subList(3, 7);
   
   System.out.println("Sublist 3,7: "+li1);
	
	 for  (Object list  : li) 
	 {
    	 System.out.print(list);
     }
	}
}
