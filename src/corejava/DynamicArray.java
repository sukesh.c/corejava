package corejava;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DynamicArray {
    public static void main(String[] args) {
        //1.Initialize
        List <Integer>  l1= new ArrayList<>();
        List <Integer> l2 ;
        //2. Cast Array to ArrayList using utility class Arrays
         Integer a [] = {23,21,45,23,44};
        l1= new ArrayList<>(Arrays.asList(a));
        // 3 . Make a  Copy
        List<Integer> l3 =l1;
        //4. Get length
        System.out.println("Length list.size of  First List : "+l1.size());
        // 5. Access Element
        System.out.println("First element in First List.get(<index>) : "+l1.get(0));
        //6. Iterate through the  ArrayList
        System.out.println("Version 1 using forloop and size");
        for(int i=0;i<l1.size();i++){
            System.out.println( "Elements  at "+ i + " :" +l1.get(i));

        }
        System.out.println("version 2 method using for each ");
         for(int item : l1)
             System.out.println("Element  is "+ item );
    // 7 . modify the List
            l1.set(2,39);
        System.out.println("Modified  List ");
        for(int item : l1)
            System.out.println("Element  is "+ item );
     //8 : Sort
        Collections.sort(l1);
        System.out.println("Sorted  List ");
        for(int item : l1)
            System.out.println("Element  is "+ item );
        //9 : Add  end  of  list
          l1.add(98);
          //10 .Add particular  place
         l1.add(3,100);
        System.out.println("New  list after additiong");
        for(int item : l1)
            System.out.println("Ele :   "+item);

        //11: delete  element  at the last
        l1.remove(l1.size()-1);
        System.out.println("list after delete");
        for(int item : l1)
            System.out.println("Elements now  :" + item);
        }
    }

